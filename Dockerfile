FROM node:16

WORKDIR /usr/src/app
COPY app/* ./

# TODO: Do not run the app as root inside the container

EXPOSE 3000
CMD node server.js
