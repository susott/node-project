#!/usr/bin/env groovy

def buildApp() {
    dir("app") {
        sh 'npm install'
        // check that a node_modules folder gets created and that package-lock.json is updated?
        // npm run build will only do something if it's defined manually what to build in package.json.
    }
}

def incrementVersion() {
  // steps for the version increment:
    // 1. Jenkins checks out current git branch with current version (from package.json)
    // 2. We do the version increment here (and use it for docker version, too)
    // 3. push that version change back to git repo (if tests passed)
    // 4. Trigger the build automatically on git push, but not if only the version got changed
  dir("app") {
      echo 'incrementing version...'
      sh 'npm version patch'
      def matcher = readFile('package.json') =~ '"version": "(.+)",'
      def version = matcher[0][1]
      echo 'hello world'
      env.IMAGE_NAME = "$version-$BUILD_NUMBER"
      echo 'hello again'
  }
}

def testApp() {
    dir("app") {
        echo 'hello hello'
        sh 'npm run test'
        // Question: Will the following stages fail automatically if tests are not passsing?
    }
}

def deployApp() {
    echo 'build docker image'
    sh 'docker build -t "3257n12iyasdfzaszewr/my-repo:node-app_${IMAGE_NAME}" .'
    sh 'docker images'
    withCredentials([usernamePassword(
      credentialsId: 'dockerhub-repo',
      passwordVariable: 'PASS',
      usernameVariable: 'USER'
    )]) {
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push 3257n12iyasdfzaszewr/my-repo:node-app_${IMAGE_NAME}"
        // sh 'docker push "3257n12iyasdfzaszewr/my-repo:node-app_${IMAGE_NAME}"'
        echo "pushing Version ${IMAGE_NAME} to dockerhub"
    }
}

def versionToGitlab() {
    withCredentials([usernamePassword(
      credentialsId: 'gitlab',
      passwordVariable: 'PASS',
      usernameVariable: 'USER'
    )]) {
        // only needed once, but let's keep it for now
        sh 'git config user.email "foo@example.com"'
        sh 'git config user.name "jenkins"'

        sh "git remote set-url origin https://$USER:$PASS@gitlab.com/susott/node-project.git"
        sh 'git add .'
        sh 'git commit -m "ci: update version"'
        sh 'git push origin HEAD:master'
        echo "pushing version ${IMAGE_NAME} to github"
    }
}

return this
